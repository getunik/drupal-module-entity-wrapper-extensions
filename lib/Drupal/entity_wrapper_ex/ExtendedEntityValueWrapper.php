<?php

namespace Drupal\entity_wrapper_ex;

class ExtendedEntityValueWrapper extends \EntityValueWrapper
{
	use CommonWrapperExtensions;

	public function __construct($type, $data = NULL, $info = array())
	{
		$info = $info + array('langfallback' => FALSE);
		parent::__construct($type, $data, $info);
	}
}
