<?php

namespace Drupal\entity_wrapper_ex;

class ExtendedEntityDrupalWrapper extends \EntityDrupalWrapper
{
	use CommonWrapperExtensions;

	public function __construct($type, $data = NULL, $info = array())
	{
		$info = $info + array('langfallback' => FALSE);
		parent::__construct($type, $data, $info);

		$this->language($info['language']);
	}
}
