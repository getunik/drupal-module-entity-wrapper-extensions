<?php

namespace Drupal\entity_wrapper_ex;

class ExtendedEntityWrapperFactory extends \EntityWrapperFactory
{
	public function getWrapper($type, $data, array $info)
	{
		global $language_content;

		// if there is no $info or no language available, always set the entity metadata wrapper
		// language to the current content language
		if (!isset($info['language']))
		{
			$info['language'] = $language_content->language;
		}
		return parent::getWrapper($type, $data, $info);
	}

	protected function getEntityWrapper($type, $data, array $info)
	{
		return new ExtendedEntityDrupalWrapper($type, $data, $info);
	}

	protected function getListWrapper($type, $data, array $info)
	{
		return new ExtendedEntityListWrapper($type, $data, $info);
	}

	protected function getStructureWrapper($type, $data, array $info)
	{
		return new ExtendedEntityStructureWrapper($type, $data, $info);
	}

	protected function getValueWrapper($type, $data, array $info)
	{
		return new ExtendedEntityValueWrapper($type, $data, $info);
	}
}
