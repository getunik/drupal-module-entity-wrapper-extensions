<?php

namespace Drupal\entity_wrapper_ex;

trait CommonWrapperExtensions
{
	public function renderField($display)
	{
		if (!isset($this->info['parent'])) {
			throw new EntityMetadataWrapperException('Missing parent');
		}
		if (!$this->dataAvailable() && isset($this->info['parent'])) {
			throw new EntityMetadataWrapperException('Missing data values.');
		}

		$parent = $this->info['parent'];
		return field_view_field($parent->type(), $parent->value(), $this->info['name'], $display);
	}

	public function hasValue()
	{
		$value = $this->value();
		return !empty($value);
	}
}
