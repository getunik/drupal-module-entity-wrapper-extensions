# Overview

This module mostly addresses the issue of multilanguage support that is mostly missing in the stock entity API metadata wrappers. It also includes the basis of an extendable wrapper class framework and adds some useful additional functions to the default metadata wrappers.

# Dependencies

## Classloader
[https://www.drupal.org/project/classloader](https://www.drupal.org/project/classloader)

## Entity
This module depends on some customizations of the stock [entity](https://www.drupal.org/project/entity) module which is the basis for the extensions provided in this module. The customized version of the entity module can be found under [https://bitbucket.org/getunik/drupal-module-entity-api](https://bitbucket.org/getunik/drupal-module-entity-api). It is based on a patch provided for [issue 2057429](https://www.drupal.org/node/2057429) and also contains a patch for [issue 2335357](https://www.drupal.org/node/2335357) which - in combination with this module - allows language fallback behavior override.

# Installation

```
# cd to your sites/all/modules directory (or any subdirectory of your chosing), then do the following

$ git clone -b 7.x-1.x git@bitbucket.org:getunik/drupal-module-entity-api.git entity
$ git clone -b v-1.0.0 https://bitbucket.org/getunik/drupal-module-entity-wrapper-extensions/ entity_wrapper_ex
$ drush en entity entity_wrapper_ex
```

# Future Plans

* support for _injecting_ custom methods into all or only some of the wrappers
